import os
import shutil
import errno
import time
import configparser
import sys


def copyanything(src, dst):
    try:
        shutil.copytree(src, dst)
        # print("succes from copytree()")
    except OSError as exc:
        if exc.errno == errno.ENOTDIR:
            shutil.copy(src, dst)
            # print("success from copy()")
        else:
            raise


def clean_up(dest):
    print("Cleaning up...")
    file_list = os.listdir(dest)
    for file in file_list:
        file = dest + file
        laccdb_file = file.replace(".accdb", ".laccdb")
        # if file.endswith(".accdb") and not os.path.isfile(laccdb_file):  # file is not open
        os.remove(file)
        print(file, "Removed")
    print("Finished cleaning ...")


def mainloop(exefile):
    print("mainloop started...")
    for i in range(100):
        # user_exe = exefile.replace(".accdb", "%s.accdb" % i)
        if not os.path.isfile(exefile):  # if user_exe file doesnt exist, copy it and start it
            copyanything(source, exefile)
            os.startfile(exefile)
            return


try:
    config = configparser.ConfigParser()
    if getattr(sys, 'frozen', False):
        # module_dir = sys._MEIPASS
        module_dir = os.path.dirname(sys.executable)
    else:
        module_dir = os.path.dirname(__file__)
    print("Module dir:", module_dir)
    config_dir = os.path.join(module_dir, "Version.ini")

    config.read(config_dir)
    currentfe = config['FrontEnd']['CurrentFE']

    source = module_dir + "/" + currentfe
    dest = "C:/AccessDBFE/"
    tempfile = dest + currentfe.replace("accdb", "laccdb")
    exefile = dest + currentfe

    if not os.path.isdir(dest):
        os.makedirs(dest)
    clean_up(dest)
    if os.path.isfile(currentfe):
        mainloop(exefile)
    else:
        print("couldn't find source")

except Exception as e:
    print(e)
    input("press Enter to close....")
