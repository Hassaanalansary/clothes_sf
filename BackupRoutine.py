import configparser
import os, sys, datetime
import shutil, errno


def copyanything(src, dst):
    try:
        shutil.copytree(src, dst)
        # print("succes from copytree()")
    except OSError as exc:  # python >2.5
        if exc.errno == errno.ENOTDIR:
            shutil.copy(src, dst)
            # print("success from copy()")
        else:
            raise


today = datetime.datetime.today()
today = datetime.datetime.date(today)

config = configparser.ConfigParser()
if getattr(sys, 'frozen', False):
    # module_dir = sys._MEIPASS
    module_dir = os.path.dirname(sys.executable)
else:
    module_dir = os.path.dirname(__file__)
print("Module dir:", module_dir)
config_dir = os.path.join(module_dir, "Settings.ini")

config.read(config_dir)  # read from the ini file
current_be = config['BackEnd']['CurrentBE']
backup_dir = config['BackEnd']['Backup_Dir']
source = module_dir + "/" + current_be
dest = backup_dir + current_be
dest = dest.replace(".accdb", "_%s.accdb" % today)  # appending the date to the file name
copyanything(source, dest)
