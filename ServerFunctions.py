import atexit
import configparser
import datetime
import json
import logging
import os
import smtplib
import sys
import time
import re
from collections import namedtuple

import pypyodbc
import twilio
import win32com.client  # outlook
from twilio.rest import Client

from CloverAPI import CloverClient


class reg(object):
    def __init__(self, cursor, row):
        for (attr, val) in zip((d[0] for d in cursor.description), row):
            setattr(self, attr, val)


class Functions:
    def __init__(self):
        db_file = self.get_db_file().currentfe
        account_sid = self.get_db_file().account_sid
        auth_token = self.get_db_file().auth_token
        clover_merchant = self.get_db_file().clover_merchant
        clover_auth = self.get_db_file().clover_auth
        email = str(self.get_db_file().email)
        email_pass = str(self.get_db_file().email_pass)
        db_user, db_password = '', ''
        db_conn_str = 'DRIVER={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=%s;UID=%s;PWD=%s' % (
            db_file, db_user, db_password)
        self.client = Client(account_sid, auth_token)
        self.clover = CloverClient(clover_merchant, clover_auth)
        self.now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        LOG_FORMAT = "%(levelname)s %(asctime)s - %(message)s"
        logging.basicConfig(filename="ServerLog.log", format=LOG_FORMAT, )
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.WARNING)
        self.logger.addHandler(logging.StreamHandler())

        try:
            self.conn = pypyodbc.connect(db_conn_str)
            self.cursor = self.conn.cursor()

            self.email_server = smtplib.SMTP("smtp.gmail.com", 587)
            self.email_server.starttls()
            self.email_server.login(email, email_pass)
            atexit.register(self.exit_handler)
        except Exception as error:
            self.logger.error(error)

    def run_forever(self):
        while True:
            self.receive_sms()
            self.send_sms()
            self.update_sms_status()
            self.send_email()
            self.receive_email()
            self.add_new_message()
            self.add_clover_item()
            self.new_survey_crawler()
            self.logger.warning("Refresh.....")
            time.sleep(15)

    def run_one_time(self):
        self.receive_sms()
        self.send_sms()
        self.update_sms_status()
        self.send_email()
        # self.receive_email()
        self.add_new_message()
        self.add_clover_item()
        # self.logger.warning("Refresh.....")

    def get_db_file(self):
        config = configparser.ConfigParser()
        if getattr(sys, 'frozen', False):
            # module_dir = sys._MEIPASS
            module_dir = os.path.dirname(sys.executable)
        else:
            module_dir = os.path.dirname(__file__)
        config_dir = os.path.join(module_dir, "Version.ini")
        config.read(config_dir)
        currentfe = config['FrontEnd']['CurrentFE']
        currentfe = module_dir + "/" + currentfe
        account_sid = config['TwilioInfo']['account_sid']
        auth_token = config['TwilioInfo']['auth_token']
        email = config['Email']['Email']
        email_pass = config['Email']['Password']
        clover_merchant = config['Clover']['merchant_id']
        clover_auth = config['Clover']['auth_token']
        Output = namedtuple('Output',
                            ['currentfe', 'account_sid', 'auth_token', 'email', 'email_pass', 'clover_merchant',
                             'clover_auth'])
        output = Output(currentfe, account_sid, auth_token, email, email_pass, clover_merchant, clover_auth)
        return output

    def my_numbers(self):
        """"Return a list of available number from Twilio"""
        numbers = []
        numbers_sids = self.client.incoming_phone_numbers.list()
        for number in numbers_sids.__iter__():
            numbers.append(number.phone_number)
        return numbers

    def db_messages(self):
        """return all message in the database in the form of list of namedtuple [(1,"x","y"),(2,"z","v")]"""
        try:
            keys = ["id", "order_id", "sid", "date_placed", "via", "type", "status", "body", "customerNumber",
                    "ourNumber",
                    "serial"]
            Message = namedtuple('Messages', keys)
            messages = []
            self.cursor.execute(
                """SELECT ID, OrderID, SID, DatePlaced, Via, Type, Status, Body, CustomerNumber, OurNumber, Serial
                 FROM Messages""")
            for row in self.cursor.fetchall():
                message = Message(*row)
                messages.append(message)
            return messages
        except Exception as error:
            self.logger.error(error)

    def receive_sms(self):
        """Check new SMSs at Twilio and add them to the database"""
        messages = self.client.messages.list()
        db_sids = [d.sid for d in self.db_messages()]
        for m in messages:
            mbody = m.body.replace("'", "")  # to avoid "'" problems, when parsing the string
            mbody = self.remove_emoji(mbody)
            if (m.sid not in db_sids) and (m.status == "received") and (m.direction == "inbound"):
                sql = """SELECT ID , CustomerNumber, OurNumber FROM OrderPhones_Query 
                WHERE OurNumber='%i' and CustomerNumber='%i' and StatusID=3 """ % (int(m.to[2:]), int(m.from_[2:]))
                self.cursor.execute(sql)
                row = self.cursor.fetchone()
                # self.logger.warning(f"Message received from {row[0]}  ")
                if row is not None:
                    # self.logger.warning(self.cursor.fetchall())
                    order_id = row[0]
                    self.logger.warning(f"Message received from {row[0]} to Order# {order_id}")
                else:
                    self.logger.warning("the message didn't match any order")
                    order_id = 0

                sql = """INSERT INTO Messages (OrderID, SID, DatePlaced, Via, Type, Status, Body, CustomerNumber, OurNumber)
                                        Values(%i, '%s', #%s#, '%s', '%s', '%s', '%s', '%s', '%s')""" % (
                    order_id, m.sid, self.now, "SMS", "Receive", m.status, mbody, m.to[2:], m.from_[2:])
                try:
                    self.cursor.execute(sql)
                    self.cursor.execute("SELECT @@IDENTITY")
                    message_id = self.cursor.fetchone()[0]
                    self.conn.commit()
                    self.logger.warning(f'adding message #{message_id}')
                    if m.body in ["1", "2", "3"]:
                        self.update_order_status(message_id)
                    else:
                        self.sms_wrong_response(message_id)
                except pypyodbc.Error as error:
                    if error.args[0] == "23000":
                        self.logger.error("OrderID_OurNumber Index Violated in MessagesTable")
                        self.logger.error("From:", m.from_, ",To:", m.to, ",body:", m.body)
                    else:
                        self.logger.error(error)
                except Exception as error:
                    self.logger.error(error)

    def send_sms(self):
        """Look for 'New' SMSs in Messages Table and send them"""
        self.cursor.execute(
            "SELECT ID, CustomerNumber, OurNumber, Body FROM Messages WHERE (Via = 'SMS' and Type= 'Send' and Status='New')")

        for row in self.cursor.fetchall():
            self.logger.warning("send_sms() has found an unsend message, try to send ...")
            message_id, customer_number, our_number, body = row[0:4]
            customer_number = "+1%s" % customer_number
            our_number = "+1%s" % our_number
            self.logger.warning(row)
            self.logger.warning("Sending to %s ...." % customer_number)
            try:
                message = self.client.messages.create(
                    to=customer_number,  # "+201149769361"
                    from_=our_number,  # "+13613362929"
                    body=body
                )
                mysid = message.sid
                self.logger.warning("%s sent" % message_id)
                # update the Sid, Status to the message
                self.cursor.execute(
                    "UPDATE Messages SET SID='%s', Status='%s' WHERE ID=%i" % (mysid, message.status, message_id))
                self.logger.warning("Updated Message Status")
                self.cursor.commit()
            except twilio.rest.TwilioException as Err:
                self.logger.error("message not sent")
                self.logger.error(Err)
                self.cursor.execute(
                    "UPDATE Messages SET Status='%s' WHERE ID=%i" % ("error", message_id))
                self.cursor.commit()
                self.logger.error("message status saved as 'Error'")
            except Exception as error:
                self.logger.error(error)

    def sms_wrong_response(self, message_id):
        """If message has been received and its body is not 1,2 or 3,
        add a new message prompting the customer about the wrong response """
        self.logger.warning("sms_wrong_response has started")
        sql = "SELECT Body FROM Templates WHERE Type='Wrong Response'"
        self.cursor.execute(sql)
        body = self.cursor.fetchone()[0]

        sql = """INSERT INTO Messages
                (OrderID, SID, DatePlaced, Via, Type, Status, Body, CustomerNumber, OurNumber, Serial)
                SELECT OrderID, SID, #%s#, Via, 'Send', 'new', '%s', OurNumber, CustomerNumber, 0
                FROM Messages Where ID=%i""" % (self.now, body, message_id)
        self.cursor.execute(sql)
        self.cursor.commit()
        self.logger.warning("'Wrong Response message has been added")
        self.logger.warning(sql)

    def update_sms_status(self, ):
        """Follow up on the messages that are still pending (Queued or Sent) but not yet delivered"""
        for db_message in self.db_messages():
            if (db_message.sid is not None) and ((db_message.status == "queued") or (db_message.status == "Sent")):
                message = self.client.messages.get(db_message.sid)
                message.fetch()
                self.cursor.execute(
                    "UPDATE Messages Set Status= '%s' WHERE ID=%i" % (message.fetch().status, db_message.id))
                self.logger.warning("Message %s status changed from %s to %s" % (
                    db_message.id, db_message.status, message.fetch().status))
                self.cursor.commit()

    def update_order_status(self, message_id: int):
        """Update order Status according to the customer response"""
        self.logger.warning("update_order_status() has run")
        sql = "SELECT Body, OrderID DatePlaced, FROM Messages WHERE ID=%i" % message_id
        self.cursor.execute(sql)
        body, order_id, response_date = self.cursor.fetchall()[0]
        if order_id == 0:
            # order_id returned as 0 if not found, we will show error and stop resuming
            self.logger.error(f"No matching order for message# {message_id}")
            return
        cash_credit_sql = f"SELECT CashTotal, CreditTotal FROM OrderAmount_Query WHERE ID={order_id}"
        self.cursor.execute(cash_credit_sql)
        cash_total, credit_total = self.cursor.fetchone()
        sql_fixed_part = f"ResponseDate={response_date}, OfferedBy=1"  # offered by system
        order_sql = ""
        if body == '1':  # Accept cash
            order_sql = f"UPDATE Orders SET StatusID=5, OfferStatusID=2, CashAmount={cash_total},{sql_fixed_part} " \
                        f"WHERE ID={order_id} "
            comment_sql_filler = "Accepted CASH"
        elif body == '2':  # accept credit
            order_sql = f"UPDATE Orders SET StatusID=5, OfferStatusID=3, CreditAmount={credit_total},{sql_fixed_part} " \
                        f"WHERE ID ={order_id} "
            comment_sql_filler = "Accepted CREDIT"
        elif body == '3':  # refuse
            order_sql = f"UPDATE Orders SET StatusID=6, OfferStatusID=4, {sql_fixed_part} WHERE ID ={order_id}"
            comment_sql_filler = "REFUSED Offer"
        else:
            self.logger.warning("response is not 1 or 2 or 3")
            return

        self.cursor.execute(f"SELECT CustomerID From Orders WHERE ID={order_id}")
        customer_id = self.cursor.fetchone()[0]
        comment_sql = f"""INSERT INTO CustomerNotes (CustomerID, [Note], EmployeeID, OrderID) 
                       Values({customer_id}, 'SYSTEM: Customer {comment_sql_filler} via TEXT', 1, {order_id})"""
        self.cursor.execute(order_sql)
        self.cursor.execute(comment_sql)
        self.cursor.commit()
        self.logger.warning(f"Customer {comment_sql_filler} for Order#{order_id} via TEXT, Status has been updated")
        self.update_tub_type(order_id)

    def update_tub_type(self, order_id):
        """Update tubs type according to offer response"""
        if order_id != 0:

            self.logger.warning("update_tub_type() has run")
            sql = "SELECT OfferStatusID FROM Orders WHERE ID=%i" % order_id
            self.cursor.execute(sql)
            offer_status = self.cursor.fetchall()[0]
            tub_sql = ""
            if offer_status == 2 or 3:
                tub_sql = "UPDATE Tubs SET TypeID= 3 WHERE TypeID=4 and OrderID=%i" % order_id
            elif offer_status == 4:
                tub_sql = "UPDATE Tubs SET TypeID= 5 WHERE TypeID=4 and OrderID=%i" % order_id
            self.logger.warning(f"Tub {tub_sql} has been updated")
            self.cursor.execute(tub_sql)
            self.cursor.commit()

    def send_email(self, ):
        """Send 'New' Emails in Messages Table"""
        self.cursor.execute(
            "SELECT ID, CustomerNumber, OurNumber, Body, OrderID FROM Messages WHERE (Via = 'Email' and Type= 'Send' and Status='New')")
        for row in self.cursor.fetchall():
            message_id, customer_number, our_number, body, order_id = row[0:5]
            self.logger.warning(row)
            self.logger.warning("Sending to %s ...." % customer_number)

            subj = "Sproutfitters OrderID: %s" % order_id
            date = datetime.datetime.today()  # '2/1/2010'
            message_text = body
            msg = "From: %s\nTo: %s\nSubject: %s\nDate: %s\n\n%s" % (
                our_number, customer_number, subj, date, message_text)

            try:
                self.email_server.sendmail(our_number, customer_number, msg)
                self.logger.warning('email has been successfully sent to %s' % customer_number)
                self.cursor.execute(
                    "UPDATE Messages SET Status='delivered' WHERE ID=%i" % message_id)
                self.logger.warning("Updated Message Status")
                self.cursor.commit()
            except Exception as error:
                logging.error("can't send the Email")
                logging.error(error)

    def receive_email(self):
        outlook = win32com.client.Dispatch("Outlook.Application").GetNamespace("MAPI")
        inbox = outlook.GetDefaultFolder(6)
        messages = inbox.Items
        messages.Sort("[ReceivedTime]", True)
        db_sids = [d.sid for d in self.db_messages()]
        for m in messages:
            if m.entryid not in db_sids and m.Unread:
                mbody = m.body.split("\r\n")[0]  # get the first line of the message only
                mbody = mbody.replace("'", "")  # to avoid "'" problems, when parsing the string
                if "OrderID" in m.subject:
                    order_id = m.subject.split("OrderID:")[1]  # get the orderID from the message subject
                    order_id = order_id.strip(" ")
                    if order_id.isdigit():
                        order_id = int(order_id)
                        self.logger.warning("Email matched order%s" % order_id)
                        sql = """INSERT INTO Messages (OrderID, SID, DatePlaced, Via, Type, Status, Body, CustomerNumber, OurNumber)
                                                Values(%i, '%s', #%s#, '%s', '%s', '%s', '%s', '%s', '%s')""" % (
                            order_id, m.entryid, self.now, "Email", "Receive", "received", mbody, m.SenderEmailAddress,
                            m.to)
                        try:
                            self.cursor.execute(sql)
                            self.cursor.execute("SELECT @@IDENTITY")
                            message_id = self.cursor.fetchone()[0]
                            self.conn.commit()
                            self.logger.warning(f"messageID {message_id}")
                            self.update_order_status(message_id)
                        except pypyodbc.Error as error:
                            if error.args[0] == "23000":
                                logging.error("From:", m.sender, ",To:", m.to, ",body:", m.body)
                            self.logger.error(error)

    def exit_handler(self):
        self.logger.warning("closing connections.....")
        self.cursor.close()
        self.conn.close()
        self.email_server.quit()

    def add_new_message(self):
        """Add new message, when we don't receive a response from the customer for the previous message. up to 3 messages"""
        if datetime.datetime.now().time() > datetime.time(12):  # only run if the time is after noon

            today = datetime.datetime.today().date()
            sql = "SELECT ID FROM Orders WHERE StatusID=3"  # 3=sorted
            self.cursor.execute(sql)
            active_orders = []
            for order in self.cursor.fetchall():
                active_orders.append(order[0])
            for msg in self.db_messages():
                if (msg.serial is not None and
                        msg.date_placed.date() < today and
                        msg.type == 'Send' and
                        msg.order_id in active_orders and
                        msg.status != ("no response" or "error")):
                    if msg.serial < 3:
                        self.logger.warning(
                            "%s order%s didn't receive response. last message%s sent at %s had serial %s" % (
                                self.now, msg.order_id, msg.id, msg.date_placed, msg.serial))
                        sql = """INSERT INTO Messages
                                (OrderID, SID, DatePlaced, Via, Type, Status, Body, CustomerNumber, OurNumber, Serial)
                                SELECT OrderID, SID, #%s#, Via, Type, 'new', Body, CustomerNumber, OurNumber, %i
                                FROM Messages Where ID=%i""" % (self.now, msg.serial + 1, msg.id)
                        self.logger.warning(sql)

                        update_sql = "UPDATE Messages SET Status='no response' WHERE ID=%i" % msg.id
                        self.logger.warning(update_sql)
                        self.cursor.execute(sql)
                        self.cursor.execute(update_sql)
                        self.cursor.commit()
                        self.logger.warning("message added to order%s with serial%s" % (msg.order_id, msg.serial + 1))
                    elif msg.serial == 3:
                        update_sql = "UPDATE Messages SET Status='no response' WHERE ID=%i" % msg.id

                        self.logger.warning("Message %s status changed to 'no response'" % msg.id)
                        self.logger.warning(update_sql)
                        self.cursor.execute(update_sql)
                        self.cursor.commit()

    def add_clover_item(self):
        """Check if Items have been added to clover, and add them"""
        sql = "SELECT * FROM Clover_ItemStatus_Query WHERE Clovered = False and TypeID= 3"  # type '3' =Owned
        cursor1 = self.conn.cursor()
        cursor2 = self.conn.cursor()
        cursor1.execute(sql)
        db_items = cursor1.fetchall()
        for row in db_items:
            item = reg(cursor1, row)
            if item.id is not None:
                self.logger.warning(
                    "Add_clover_item() found an item that has not been added to clover, Trying to add... ")
                category, name = item.description.split('. ', 1)
                price = int(item.tagprice * 100)
                code = item.level7
                qty = int(item.qty)
                clover_id = item.cloverid
                if clover_id == "" or clover_id is None:
                    new_item = self.clover.add_item(name, price, code=code, isrevenue=True, quantity=qty)

                    if new_item[0] is True:
                        self.logger.warning(
                            "Item %s Level7ID %s has been add to clover successfully, Under CloverID=%s " % (
                                item.id, code, new_item[1]))
                        sql = "UPDATE Level7 SET CloverID ='%s' WHERE ID=%i" % (new_item[1], code)
                        cursor2.execute(sql)
                        cursor2.commit()
                        sql = "UPDATE Items SET Clovered = True WHERE ID=%i" % item.id
                        cursor2.execute(sql)
                        cursor2.commit()
                        self.attach_category_item(category, new_item[1])
                        time.sleep(.3)  # to avoid Error : "Too Many Requests"
                        self.add_clover_item()  # will rerun the function so we dont wait between adding
                        return  # so the query can get the newly updated items
                    else:
                        self.logger.error(
                            "this item couldn't be added to clover. Level7ID:%s. ErrorMessage:%s" % (code, new_item[1]))
                else:
                    update_quantity = self.clover.add_quantity(clover_id, qty)
                    if update_quantity is True:
                        sql = "UPDATE Items SET Clovered = True WHERE ID=%i" % item.id
                        cursor2.execute(sql)
                        cursor2.commit()
                        self.logger.warning(
                            "Item %s Level7ID %s already existed with CloverID = %s, Quantity has been raised by %i" % (
                                item.id,
                                code, clover_id,
                                qty))
                        time.sleep(.3)  # to avoid Error : "Too Many Requests"
                        self.add_clover_item()  # will rerun the function so we dont wait between adding
                        return  # so the query can get the newly updated items
                    else:
                        self.logger.error("Item %s Level7ID %s quantity didn't upload correctly,"
                                          "%s has not been found on clover. Maybe because This item has been deleted from clover " % (
                                              item.id, code, clover_id))

    def get_category_id(self, category_name):
        """Return Category CloverID"""
        categories = json.loads(self.clover.get_categories())
        for category in categories['elements']:
            if category['name'] == category_name:
                return category['id']

    def attach_category_item(self, category_name, item_id):
        """Attach Category to Entered Item"""
        self.logger.warning("trying to attach category %s to item %s....." % (category_name, item_id))
        category_id = self.get_category_id(category_name)
        self.clover.attach_category(item_id, category_id)
        self.logger.warning("Category attached successfully.")

    def new_survey_crawler(self):
        """search for new call survey to do, and reteive answer for ended ones"""
        sql = "SELECT ID, SID, CustomerNumber, OurNumber, Body, OrderID FROM Messages " \
              "WHERE (Via = 'Voice' and Type= 'Send' and Status='active')"
        self.cursor.execute(sql)
        for row in self.cursor.fetchall():
            active_message_id, active_sid = row[0:2]
            self.logger.warning(f"Checking call #{active_message_id} status....")
            answer = self.retrieve_answer(active_sid)
            if answer == "active":
                # call is still in progress, stop execution
                self.logger.warning(f"Call #{active_message_id} still in progress....")
                return
            else:
                # call ended, update message with answer
                self.logger.warning(f"Call ended with response: {answer}")
                self.cursor.execute(f"UPDATE Messages SET SID='{active_sid}',Status='{answer}' "
                                    f"WHERE ID={active_message_id}")
                # fixme update order and tubs
                self.cursor.commit()

        # continue fetching new call surveys
        sql = "SELECT ID, CustomerNumber, OurNumber, Body, OrderID FROM Messages " \
              "WHERE (Via = 'Voice' and Type= 'Send' and Status='New')"
        self.cursor.execute(sql)
        for row in self.cursor.fetchall():
            message_id, customer_number, our_number, body, order_id = row[0:5]
            self.logger.warning(row)
            self.logger.warning("Calling %s ...." % customer_number)
            exec_sid = self.survey_call(customer_number, our_number, body)
            self.cursor.execute(f"UPDATE Messages SET SID= '{exec_sid}', Status='active' WHERE ID={message_id}")
            self.cursor.commit()
            return

    def survey_call(self, customer_number, our_number, message_body):
        """Start a survey call and return the call's sid"""
        cash_msg = self.cursor.execute("SELECT Body From Templates WHERE Type='VoiceCall_AcceptCash'").fetchone()[0]
        credit_msg = self.cursor.execute("SELECT Body From Templates WHERE Type='VoiceCall_AcceptCredit'").fetchone()[0]
        refuse_msg = self.cursor.execute("SELECT Body From Templates WHERE Type='VoiceCall_RefuseOffer'").fetchone()[0]
        survey = self.client.studio.flows('FWc65911422580ecb6e97b34afd565eca4').executions
        execution = survey.create(to=customer_number,
                                  from_=our_number, parameters={
                'msg_body': message_body, 'cash_msg': cash_msg, 'credit_msg': credit_msg, 'refuse_msg': refuse_msg})
        return execution.sid

    def retrieve_answer(self, exec_sid):
        """return the outcome of the survey call"""
        # fixme catch all possible outputs
        x = self.client.studio.flows('FWc65911422580ecb6e97b34afd565eca4').executions.get(exec_sid)
        call_status = x.fetch().status
        step_list = x.steps.list(1)
        response = step_list[0].transitioned_from
        if call_status == "ended":
            return response
        else:
            return call_status

    def remove_emoji(self, string):
        """ to remove emojis when we receive an sms because emojis can't inserted in Access for some reason"""
        emoji_pattern = re.compile("["
                                   u"\U0001F600-\U0001F64F"  # emoticons
                                   u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                                   u"\U0001F680-\U0001F6FF"  # transport & map symbols
                                   u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                                   u"\U00002702-\U000027B0"
                                   u"\U000024C2-\U0001F251"
                                   "]+", flags=re.UNICODE)
        return emoji_pattern.sub(r'', string)


if __name__ == '__main__':
    """for testing purposes"""
    # Functions().new_survey_crawler()
    # Functions().survey_call("+201149769361", "+16363031935", 55,
    #                         "Hi this is sproutfitters children's resale. how are you hassaan")
    # Functions().receive_sms()

# fixme close connection upon termination
# todo mark orders as 'Abandoned' if all three message has status='no response'
