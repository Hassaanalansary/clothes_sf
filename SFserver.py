import atexit
import configparser
import datetime
import json
import logging
import os
import smtplib
import sys
import time
from collections import namedtuple

import pypyodbc
import twilio
import win32com.client  # outlook
from twilio.rest import Client

from CloverAPI import CloverClient
import re


def remove_emoji(string):
    emoji_pattern = re.compile("["
                               u"\U0001F600-\U0001F64F"  # emoticons
                               u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                               u"\U0001F680-\U0001F6FF"  # transport & map symbols
                               u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                               u"\U00002702-\U000027B0"
                               u"\U000024C2-\U0001F251"
                               "]+", flags=re.UNICODE)
    return emoji_pattern.sub(r'', string)


class reg(object):
    def __init__(self, cursor, row):
        for (attr, val) in zip((d[0] for d in cursor.description), row):
            setattr(self, attr, val)


def get_db_file():
    config = configparser.ConfigParser()
    if getattr(sys, 'frozen', False):
        # module_dir = sys._MEIPASS
        module_dir = os.path.dirname(sys.executable)
    else:
        module_dir = os.path.dirname(__file__)
    config_dir = os.path.join(module_dir, "Version.ini")
    config.read(config_dir)
    currentfe = config['FrontEnd']['CurrentFE']
    currentfe = module_dir + "/" + currentfe
    account_sid = config['TwilioInfo']['account_sid']
    auth_token = config['TwilioInfo']['auth_token']
    email = config['Email']['Email']
    email_pass = config['Email']['Password']
    clover_merchant = config['Clover']['merchant_id']
    clover_auth = config['Clover']['auth_token']
    Output = namedtuple('Output', ['currentfe', 'account_sid', 'auth_token', 'email', 'email_pass', 'clover_merchant',
                                   'clover_auth'])
    output = Output(currentfe, account_sid, auth_token, email, email_pass, clover_merchant, clover_auth)
    return output


def my_numbers():
    numbers = []
    numbers_sids = client.incoming_phone_numbers.list()
    for number in numbers_sids.__iter__():
        numbers.append(number.phone_number)
    return numbers


def db_messages(cursor):
    """return all message in the database in the form of list of namedtuple [(1,"x","y"),(2,"z","v")]"""
    try:
        keys = ["id", "order_id", "sid", "date_placed", "via", "type", "status", "body", "customerNumber", "ourNumber",
                "serial"]
        Message = namedtuple('Messages', keys)
        messages = []
        cursor.execute(
            """SELECT ID, OrderID, SID, DatePlaced, Via, Type, Status, Body, CustomerNumber, OurNumber, Serial
             FROM Messages""")
        for row in cursor.fetchall():
            message = Message(*row)
            messages.append(message)
        return messages
    except pypyodbc.Error as error:
        logger.error(error)


def receive_sms(client, cursor):
    messages = client.messages.list()
    db_sids = [d.sid for d in db_messages(cursor)]
    for m in messages:
        mbody = m.body.replace("'", "")  # to avoid "'" problems, when parsing the string
        mbody = remove_emoji(mbody)
        if (m.sid not in db_sids) and (m.status == "received") and (m.direction == "inbound"):
            sql = """SELECT ID , CustomerNumber, OurNumber FROM OrderPhones_Query 
            WHERE OurNumber='%i' and CustomerNumber='%i' and StatusID=3 """ % (int(m.to[2:]), int(m.from_[2:]))
            cursor.execute(sql)
            row = cursor.fetchone()
            logger.warning(row)
            if row is not None:
                logger.warning(cursor.fetchall())
                order_id = row[0]
                logger.warning("the message received matched OrderID=", order_id)
            else:
                logger.warning("the message didn't match any order")
                order_id = 0

            sql = r"""INSERT INTO Messages (OrderID, SID, DatePlaced, Via, Type, Status, Body, CustomerNumber, OurNumber)
                                    Values(%i, '%s', #%s#, '%s', '%s', '%s', '%s', '%s', '%s')""" % (
                order_id, m.sid, now, "SMS", "Receive", m.status, mbody, m.to[2:], m.from_[2:])
            try:
                cursor.execute(sql)
                cursor.execute("SELECT @@IDENTITY")
                message_id = cursor.fetchone()[0]
                conn.commit()
                logger.warning(message_id)
                if m.body == ("1" or "2" or "3"):
                    update_order_status(cursor, message_id)
                else:
                    sms_wrong_response(message_id)
            except pypyodbc.Error as error:
                if error.args[0] == "23000":
                    logger.error("OrderID_OurNumber Index Violated in MessagesTable")
                    logger.error("From:", m.from_, ",To:", m.to, ",body:", m.body)
                logger.error(error)


def send_sms(client, cursor):
    """Look for 'New' SMSs in Messages Table and send them"""
    cursor.execute(
        "SELECT ID, CustomerNumber, OurNumber, Body FROM Messages WHERE (Via = 'SMS' and Type= 'Send' and Status='New')")

    for row in cursor.fetchall():
        logger.warning("send_sms() has found an unsend message, try to send ...")
        message_id, customer_number, our_number, body = row[0:4]
        customer_number = "+1%s" % customer_number
        our_number = "+1%s" % our_number
        logger.warning(row)
        logger.warning("Sending to %s ...." % customer_number)
        try:
            message = client.messages.create(
                to=customer_number,  # "+201149769361"
                from_=our_number,  # "+13613362929"
                body=body
            )
            mysid = message.sid
            logger.warning("%s sent" % message_id)
            # update the Sid, Status to the message
            cursor.execute(
                "UPDATE Messages SET SID='%s', Status='%s' WHERE ID=%i" % (mysid, message.status, message_id))
            logger.warning("Updated Message Status")
            cursor.commit()
        except twilio.rest.TwilioException as Err:
            logger.error("message not sent")
            logger.error(Err)
            cursor.execute(
                "UPDATE Messages SET Status='%s' WHERE ID=%i" % ("error", message_id))
            cursor.commit()
            logger.error("message status saved as 'Error'")


def sms_wrong_response(message_id):
    """If message has been received and its body is not 1,2 or 3,
    add a new message prompting the customer about the wrong response """
    logger.warning("sms_wrong_response has started")
    sql = "SELECT Body FROM Templates WHERE Type='Wrong Response'"
    cursor.execute(sql)
    body = cursor.fetchone()[0]

    sql = """INSERT INTO Messages
            (OrderID, SID, DatePlaced, Via, Type, Status, Body, CustomerNumber, OurNumber, Serial)
            SELECT OrderID, SID, #%s#, Via, 'Send', 'new', '%s', OurNumber, CustomerNumber, 0
            FROM Messages Where ID=%i""" % (now, body, message_id)
    cursor.execute(sql)
    cursor.commit()
    logger.warning("'Wrong Response message has been added")
    logger.warning(sql)


def update_sms_status(client, cursor):
    """Follow up on the messages that are still pending (Queued or Sent) but not yet delivered"""
    for db_message in db_messages(cursor):
        if (db_message.sid is not None) and ((db_message.status == "queued") or (db_message.status == "Sent")):
            message = client.messages.get(db_message.sid)
            message.fetch()
            cursor.execute("UPDATE Messages Set Status= '%s' WHERE ID=%i" % (message.fetch().status, db_message.id))
            logger.warning("Message %s status changed from %s to %s" % (
                db_message.id, db_message.status, message.fetch().status))
            cursor.commit()


def update_order_status(cursor, message_id: int):
    """Update order Status according to the customer response"""
    logger.warning("update_order_status() has run")
    sql = "SELECT Body, OrderID FROM Messages WHERE ID=%i" % (message_id)
    cursor.execute(sql)
    body, order_id = cursor.fetchall()[0]
    order_sql = ""
    if body == '1':  # Accept cash
        order_sql = "UPDATE Orders SET StatusID=5, OfferStatusID=2 WHERE ID=%i" % order_id
    elif body == '2':  # accept credit
        order_sql = "UPDATE Orders SET StatusID=5, OfferStatusID=3 WHERE ID = %i" % order_id
    elif body == '3':  # refuse
        order_sql = "UPDATE Orders SET StatusID=6, OfferStatusID=4 WHERE ID = %i" % order_id
    else:
        logger.warning("response is not 1 or 2 or 3")
        return
    cursor.execute(order_sql)
    cursor.commit()
    logger.warning("Order", order_id, "Status has been updated")
    update_tub_type(cursor, order_id)


def update_tub_type(cursor, order_id):
    """Update tubs type according to offer response"""
    if order_id != 0:

        logger.warning("update_tub_type() has run")
        sql = "SELECT OfferStatusID FROM Orders WHERE ID=%i" % order_id
        cursor.execute(sql)
        offer_status = cursor.fetchall()[0]
        tub_sql = ""
        if offer_status == 2 or 3:
            tub_sql = "UPDATE Tubs SET TypeID= 3 WHERE TypeID=4 and OrderID=%i" % order_id
        elif offer_status == 4:
            tub_sql = "UPDATE Tubs SET TypeID= 5 WHERE TypeID=4 and OrderID=%i" % order_id
            logger.warning("Tub", tub_sql, "has been updated")
        cursor.execute(tub_sql)
        cursor.commit()


def send_email(cursor, server):
    """Send 'New' Emails in Messages Table"""
    cursor.execute(
        "SELECT ID, CustomerNumber, OurNumber, Body, OrderID FROM Messages WHERE (Via = 'Email' and Type= 'Send' and Status='New')")
    for row in cursor.fetchall():
        message_id, customer_number, our_number, body, order_id = row[0:5]
        logger.warning(row)
        logger.warning("Sending to %s ...." % customer_number)

        subj = "Sproutfitters OrderID: %s" % order_id
        date = '2/1/2010'
        message_text = body
        msg = "From: %s\nTo: %s\nSubject: %s\nDate: %s\n\n%s" % (our_number, customer_number, subj, date, message_text)

        try:
            server.sendmail(our_number, customer_number, msg)
            logger.warning('email has been successfully sent to %s' % customer_number)
            cursor.execute(
                "UPDATE Messages SET Status='delivered' WHERE ID=%i" % message_id)
            logger.warning("Updated Message Status")
            cursor.commit()
        except Exception:
            logging.error("can't send the Email")
            logging.error(Exception)


def receive_email(cursor):
    outlook = win32com.client.Dispatch("Outlook.Application").GetNamespace("MAPI")
    inbox = outlook.GetDefaultFolder(6)
    messages = inbox.Items
    messages.Sort("[ReceivedTime]", True)
    db_sids = [d.sid for d in db_messages(cursor)]
    for m in messages:
        if m.entryid not in db_sids and m.Unread:
            mbody = m.body.split("\r\n")[0]  # get the first line of the message only
            mbody = mbody.replace("'", "")  # to avoid "'" problems, when parsing the string
            if "OrderID" in m.subject:
                order_id = m.subject.split("OrderID:")[1]  # get the orderID from the message subject
                order_id = order_id.strip(" ")
                if order_id.isdigit():
                    order_id = int(order_id)
                    logger.warning("Email matched order%s" % order_id)
                    sql = """INSERT INTO Messages (OrderID, SID, DatePlaced, Via, Type, Status, Body, CustomerNumber, OurNumber)
                                            Values(%i, '%s', #%s#, '%s', '%s', '%s', '%s', '%s', '%s')""" % (
                        order_id, m.entryid, now, "Email", "Receive", "received", mbody, m.SenderEmailAddress, m.to)
                    try:
                        cursor.execute(sql)
                        cursor.execute("SELECT @@IDENTITY")
                        message_id = cursor.fetchone()[0]
                        conn.commit()
                        logger.warning("messageID", message_id)
                        update_order_status(cursor, message_id)
                    except pypyodbc.Error as error:
                        if error.args[0] == "23000":
                            logging.error("From:", m.sender, ",To:", m.to, ",body:", m.body)
                            logger.error(error)


def exit_handler(cursor, conn, email_server):
    logger.warning("closing connections.....")
    cursor.close()
    conn.close()
    email_server.quit()


def add_new_message():
    """Add new message, when we don't receive a response from the customer for the previous message. up to 3 messages"""
    if datetime.datetime.now().time() > datetime.time(12):  # only run if the time is after noon

        today = datetime.datetime.today().date()
        sql = "SELECT ID FROM Orders WHERE StatusID=3"  # 3=sorted
        cursor.execute(sql)
        active_orders = []
        for order in cursor.fetchall():
            active_orders.append(order[0])
        for msg in db_messages(cursor):
            if (msg.serial is not None and
                    msg.date_placed.date() < today and
                    msg.type == 'Send' and
                    msg.order_id in active_orders and
                    msg.status != ("no response" or "error")):
                if msg.serial < 3:
                    logger.warning("%s order%s didn't receive response. last message%s sent at %s had serial %s" % (
                        now, msg.order_id, msg.id, msg.date_placed, msg.serial))
                    sql = """INSERT INTO Messages
                            (OrderID, SID, DatePlaced, Via, Type, Status, Body, CustomerNumber, OurNumber, Serial)
                            SELECT OrderID, SID, #%s#, Via, Type, 'new', Body, CustomerNumber, OurNumber, %i
                            FROM Messages Where ID=%i""" % (now, msg.serial + 1, msg.id)
                    logger.warning(sql)

                    update_sql = "UPDATE Messages SET Status='no response' WHERE ID=%i" % msg.id
                    logger.warning(update_sql)
                    cursor.execute(sql)
                    cursor.execute(update_sql)
                    cursor.commit()
                    logger.warning("message added to order%s with serial%s" % (msg.order_id, msg.serial + 1))
                elif msg.serial == 3:
                    update_sql = "UPDATE Messages SET Status='no response' WHERE ID=%i" % msg.id

                    logger.warning("Message %s status changed to 'no response'" % msg.id)
                    logger.warning(update_sql)
                    cursor.execute(update_sql)
                    cursor.commit()


def add_clover_item():
    """Check if Items have been added to clover, and add them"""
    sql = "SELECT * FROM Clover_ItemStatus_Query WHERE Clovered = False and TypeID= 3"  # type '3' =Owned
    cursor1 = conn.cursor()
    cursor2 = conn.cursor()
    cursor1.execute(sql)
    db_items = cursor1.fetchall()
    for row in db_items:
        item = reg(cursor1, row)
        if item.id is not None:
            logger.warning("Add_clover_item() found an item that has not been added to clover, Trying to add... ")
            category, name = item.description.split('. ', 1)
            price = int(item.tagprice * 100)
            code = item.level7
            qty = int(item.qty)
            clover_id = item.cloverid
            if clover_id == "" or clover_id is None:
                new_item = clover.add_item(name, price, code=code, isrevenue=True, quantity=qty)

                if new_item[0] is True:
                    logger.warning(
                        "Item %s Level7ID %s has been add to clover successfully, Under CloverID=%s " % (
                            item.id, code, new_item[1]))
                    sql = "UPDATE Level7 SET CloverID ='%s' WHERE ID=%i" % (new_item[1], code)
                    cursor2.execute(sql)
                    cursor2.commit()
                    sql = "UPDATE Items SET Clovered = True WHERE ID=%i" % item.id
                    cursor2.execute(sql)
                    cursor2.commit()
                    attach_category_item(category, new_item[1])
                    return  # so the query can get the newly updated items
                else:
                    logger.warning(
                        "this item couldn't be added to clover. Level7ID:%s. ErrorMessage:%s" % (code, new_item[1]))
            else:
                update_quantity = clover.add_quantity(clover_id, qty)
                if update_quantity is True:
                    sql = "UPDATE Items SET Clovered = True WHERE ID=%i" % item.id
                    cursor2.execute(sql)
                    cursor2.commit()
                    logger.warning(
                        "Item %s Level7ID %s already existed with CloverID = %s, Quantity has been raised by %i" % (
                            item.id,
                            code, clover_id,
                            qty))
                    return  # so the query can get the newly updated items
                else:
                    logger.warning("Items %s quantity didn't upload correctly,\n"
                                   "%s has not been found on clover. Maybe because This item has been deleted from clover " % (
                                       code, clover_id))


def get_category_id(category_name):
    """Return Category CloverID"""
    categories = json.loads(clover.get_categories())
    for category in categories['elements']:
        if category['name'] == category_name:
            return category['id']


def attach_category_item(category_name, item_id):
    """Attach Category to Entered Item"""
    logger.warning("trying to attach category %s to item %s....." % (category_name, item_id))
    category_id = get_category_id(category_name)
    clover.attach_category(item_id, category_id)
    logger.warning("Category attached successfully.")


# fixme close connection upon termination
# todo mark orders as 'Abandoned' if all three message has status='no response'

db_file = get_db_file().currentfe
account_sid = get_db_file().account_sid
auth_token = get_db_file().auth_token
clover_merchant = get_db_file().clover_merchant
clover_auth = get_db_file().clover_auth
email = str(get_db_file().email)
email_pass = str(get_db_file().email_pass)
db_user, db_password = '', ''
db_conn_str = 'DRIVER={Microsoft Access Driver (*.mdb, *.accdb)};charset=utf8mb4;DBQ=%s;UID=%s;PWD=%s' % (
    db_file, db_user, db_password)
client = Client(account_sid, auth_token)
clover = CloverClient(clover_merchant, clover_auth)
now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

LOG_FORMAT = "%(levelname)s %(asctime)s - %(message)s"
logging.basicConfig(filename="ServerLog.log", format=LOG_FORMAT, )
logger = logging.getLogger()
logger.setLevel(logging.WARNING)
logger.addHandler(logging.StreamHandler())

try:
    conn = pypyodbc.connect(db_conn_str)
    cursor = conn.cursor()

    email_server = smtplib.SMTP("smtp.gmail.com", 587)
    email_server.starttls()
    email_server.login(email, email_pass)
    atexit.register(exit_handler, cursor, conn, email_server)

    while True:
        receive_sms(client, cursor)
        send_sms(client, cursor)
        update_sms_status(client, cursor)
        send_email(cursor, email_server)
        receive_email(cursor)
        add_new_message()
        add_clover_item()
        logger.warning("Refresh.....")
        time.sleep(15)

except Exception as error:
    logger.error(error)
