from datetime import datetime, timedelta
import os
import time


def log_append(message, level_name):
    with open("ServerLog.log", 'a') as mylog:
        now = datetime.now().strftime("%Y-%m-%d %H:%M:%S,%f")[:-3]
        mylog.write(f"{level_name} {now} - ServerInspector: {message}\n")


try:
    with open("ServerLog.log", 'r') as f:
        last_line = ""
        for line in f:
            if line.startswith("WARNING") or line.startswith("ERROR"): last_line = line
            x = last_line.split(" - ", 1)[0]
            log_time = x.split(" ", 1)[1]
            status = line.split(" ",1)[0]

        log_time = datetime.strptime(log_time, "%Y-%m-%d %H:%M:%S,%f")
        if datetime.now() > log_time + timedelta(minutes=30):
            print(f"Server has stopped since {log_time}")
            log_append("Server has not been running", "ERROR")
            os.system("TASKKILL /F /IM ServerGUI.exe")
            print("Starting Server GUI...")
            os.startfile("ServerGUI.exe")
            log_append("Server has been restarted", "ERROR")

        else:
            print("Server is still running correctly")
            log_append("Server is still running correctly", "WARNING")

except Exception as err:
    print("error occurred")
    log_append(f"ServerInspector faced this error '{err}'", "ERROR")
    print("please check the log")
    input("Press Enter to close....")
finally:
    time.sleep(3)
