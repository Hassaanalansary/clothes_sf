import sys
from PyQt5 import uic, QtWidgets, QtCore, QtGui
from ServerFunctions import Functions
import time
import logging
import qdarkstyle
from UI_Files import ServerUI
import threading


class TextHandler(logging.Handler):
    """This class allows you to log to a PyQt Text or ScrolledText widget"""

    def __init__(self, widget):
        # run the regular Handler __init__
        logging.Handler.__init__(self)
        # Store a reference to the Text it will log to
        self.widget = widget

    def emit(self, record):
        msg = self.format(record)
        self.widget.addItem(msg)
        self.widget.scrollToBottom()


class ThreadFunction(QtCore.QThread): # Not used deprecated

    def __init__(self, func, status_frame):
        QtCore.QThread.__init__(self)
        self._isRunning = True
        self.func = func
        self.status_frame = status_frame

    def run(self):
        while self._isRunning:
            # self.change_color(self.status_frame, QtCore.Qt.green)
            self.status_frame.setStyleSheet("background-color: green")
            self.func()
            time.sleep(15)

    def stop(self):
        self._isRunning = False
        self.status_frame.setStyleSheet("background-color: red")
        # self.change_color(self.status_frame, QtCore.Qt.red)

    def change_color(self, widget, Qcolor):
        w = widget
        p = w.palette()
        p.setColor(w.backgroundRole(), Qcolor)
        w.setPalette(p)


class Window(QtWidgets.QMainWindow, ServerUI.Ui_MainWindow):
    def __init__(self):
        super(Window, self).__init__()
        self.setupUi(self)
        # uic.loadUi('UI_Files/ServerUI.ui', self)
        self.show()
        self.sf = Functions()
        self.is_server_running = False
        self.adjust_logger()
        self.stop_btn.clicked.connect(self.stop_server)
        self.run_btn.clicked.connect(self.run_server)
        self.listWidget.scrollToBottom()
        # self.change_color(self.status_frame, QtCore.Qt.red)
        self.status_frame.setStyleSheet("background-color: red; color: red; font-weight: 800; font-size: 22")
        self.run_server()

    def adjust_logger(self):
        self.mylogger = self.sf.logger
        LOG_FORMAT = "%(asctime)s - %(message)s"
        handler = TextHandler(self.listWidget)
        handler.setFormatter(logging.Formatter(LOG_FORMAT))
        handler.setLevel(logging.WARNING)
        self.mylogger.addHandler(handler)

    def change_color(self, widget, Qcolor):
        w = widget
        p = w.palette()
        p.setColor(w.backgroundRole(), Qcolor)
        w.setPalette(p)

    def run_server(self):
        if self.is_server_running:
            logging.warning("Server is already running")
        else:
            self.t = threading.Thread(target=self.doit, args=("task",),
                                      daemon=True)  # creating a thread to run server in it
            self.t.start()
            self.is_server_running = True
            self.status_frame.setStyleSheet("background-color: green")

    def doit(self, arg):
        while getattr(self.t, "do_run", True):
            logging.warning("Refresh...")
            self.sf.run_one_time()
            time.sleep(15)
        logging.warning("Stopping as you wish.")

    def stop_server(self):
        try:
            self.t = threading.currentThread()
            self.t.do_run = False
            self.is_server_running = False
            self.status_frame.setStyleSheet("background-color: red")
            self.t.join()
            logging.warning("Server is stopping")

        except Exception as error:
            self.logger.error(error)

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    app_icon = QtGui.QIcon()
    app_icon.addFile('Icons/Clover.ico')
    app.setWindowIcon(app_icon)
    # app.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())
    window = Window()
    sys.exit(app.exec_())
